class FileSystemPath:
    separator: str = '/'

    def __init__(self, root):
        self.root = root

    def __str__(self):
        return self.root

    def extend(self, node):
        return FileSystemPath(self.root + self.separator + node)