from src.domain import FileSystemPath


class DataEntity:
    def __init__(self, label: str, path: FileSystemPath, file_name: str):
        self.label = label
        self.path = path
        self.file_name = file_name

    def __str__(self):
        return self.label + ', ' + str(self.path)
