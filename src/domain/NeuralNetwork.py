import torch.nn as nn


class NeuralNetwork(nn.Module):

    def __init__(self, number_of_classes: int):
        super(NeuralNetwork, self).__init__()
        self.layer_1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=11, stride=1, padding=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.layer_2 = nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer_3 = nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.drop_out = nn.Dropout()
        self.fc_1 = nn.Linear(49 * 49 * 256, 512)
        self.fc_2 = nn.Linear(512, number_of_classes)

    def forward(self, out):
        _out = self.layer_1(out)
        _out = self.layer_2(_out)
        _out = self.layer_3(_out)
        # self.number_flat_features(_out)
        _out = _out.reshape(_out.size(0), -1)
        _out = self.drop_out(_out)
        _out = self.fc_1(_out)
        _out = self.fc_2(_out)
        return _out

    @staticmethod
    def number_flat_features(x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
