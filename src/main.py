import torch
import torchvision
import torch.optim as optim
import torch.nn as nn
import time
import copy
import torchvision.models as models

from torchvision import transforms
from src.domain.FileSystemPath import FileSystemPath
from src.domain.NeuralNetwork import NeuralNetwork
from src.utils.dataset import index_files, create_train_test_folders
from src.utils.image_processor import resize_image, load_image, normalized, remove_background
from sklearn.cluster import KMeans
from collections import Counter


def load_dataset(_folder, is_train_transformer=True):
    if is_train_transformer:
        transformer = transforms.Compose([
            transforms.Resize((400, 400)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomRotation(15),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    else:
        transformer = transforms.Compose([transforms.Resize((400, 400)),
                                          transforms.ToTensor(),
                                          transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    data_path = str(_folder)

    _dataset = torchvision.datasets.ImageFolder(
        root=data_path,
        transform=transformer,
    )

    _loader = torch.utils.data.DataLoader(
        _dataset,
        batch_size=16,
        num_workers=2,
        shuffle=True
    )

    return _loader, len(_dataset)


def create_loss_and_optimizer(model, learning_rate=0.01):
    loss = torch.nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)
    return loss, optimizer


def evaluate_model(model, device, loader, scheduler):
    correct = 0.0
    total = 0.0
    with torch.no_grad():
        for i, data in enumerate(loader, 0):
            images, labels = data
            images = images.to(device)
            labels = labels.to(device)

            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)

            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    accuracy = 100.0 * correct / total
    scheduler.step(accuracy)
    print('Testing accuracy: %d' % accuracy)
    return accuracy


def train_model(model, device, loader, dataset_size, optimizer, loss_function):
    model.train()
    running_loss = 0
    running_correct = 0

    for data_input, data_label in loader:
        data_input = data_input.to(device)
        data_label = data_label.to(device)

        optimizer.zero_grad()

        data_output = model(data_input)
        _, prediction = torch.max(data_output, 1)
        loss = loss_function(data_output, data_label)

        loss.backward()
        optimizer.step()

        running_loss += loss.item() * data_input.size(0)
        running_correct += torch.sum(prediction == data_label.data)
        loss.detach()

    epoch_loss = running_loss / dataset_size
    epoch_accuracy = 100 * running_correct.double() / dataset_size

    print('Training Loss: {:.4f} Accuracy: {:.4f}'.format(epoch_loss, epoch_accuracy))
    return model


def model_training(model, device, train_folder: FileSystemPath, validation_folder: FileSystemPath, number_of_epochs=10,
                   learning_rate=0.01):

    start = time.time()
    train_loader, train_size = load_dataset(train_folder)
    validation_loader, validation_size = load_dataset(validation_folder, is_train_transformer=False)
    loss_function, optimizer = create_loss_and_optimizer(model, learning_rate)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', patience=3, threshold=0.9)

    best_model = copy.deepcopy(model.state_dict())
    best_accuracy = 0.0

    for epoch in range(number_of_epochs):
        print('Epoch {}/{}'.format(epoch, number_of_epochs - 1))
        model = train_model(model, device, train_loader, train_size, optimizer, loss_function)
        metric = evaluate_model(model, device, validation_loader, scheduler)
        scheduler.step(metric)

        if metric > best_accuracy:
            best_accuracy = metric
            best_model = copy.deepcopy(model.state_dict())

    time_elapsed = time.time() - start
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best Test Accuracy: {:4f}'.format(best_accuracy))
    model.load_state_dict(best_model)
    return model


def find_image_dominant_color(path: FileSystemPath):
    image = load_image(path)
    image = resize_image(image)
    image = normalized(image)
    image = remove_background(image)

    pixels = image.reshape((image.shape[0] * image.shape[1], 3))
    clt = KMeans(n_clusters=3)
    labels = clt.fit_predict(pixels)
    label_counts = Counter(labels)
    dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]
    return dominant_color[0]


def run():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    print(device)
    print(torch.cuda.get_device_name(device))

    train_folder = FileSystemPath('../dataset/train')
    test_folder = FileSystemPath('../dataset/test')

    train_dataset, test_dataset = index_files()
    create_train_test_folders(train_dataset, test_dataset, train_folder, test_folder)
    labels = train_dataset['label'].unique()

    # Train car brand classifier
    model = models.resnet34(pretrained=True)
    # model = NeuralNetwork(len(labels)) # Video card ram is to small to train network from scratch

    number_of_features = model.fc.in_features
    model.fc = nn.Linear(number_of_features, len(labels))
    model = model.to(device)
    model_training(model, device, train_folder, test_folder, number_of_epochs=10, learning_rate=0.01)

    # Find car color
    test_dataset['color'] = test_dataset['path'].apply(lambda path: find_image_dominant_color(FileSystemPath(path)))


if __name__ == '__main__':
    run()
