import os
import logging
from src.domain.FileSystemPath import FileSystemPath
from typing import Set


logger = logging.getLogger('main')


def list_files(path: FileSystemPath) -> Set[FileSystemPath]:
    aggregation_list: Set[FileSystemPath] = set()

    for file_name in os.listdir(str(path)):
        file_path: FileSystemPath = path.extend(file_name)
        is_directory: bool = os.path.isdir(str(file_path))

        if is_directory:
            aggregation_list = aggregation_list.union(list_files(file_path))
        else:
            aggregation_list.add(file_path)

    return aggregation_list


def create_folder(path: FileSystemPath):
    try:
        os.mkdir(str(path.root))
    except OSError:
        logger.info("directory was already created %s " % path.root)
