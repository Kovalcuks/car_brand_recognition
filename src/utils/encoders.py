import numpy as np
from src.domain.DataEntity import DataEntity
from src.domain.FileSystemPath import FileSystemPath


def encode_path_to_data_entity(path: FileSystemPath) -> DataEntity:
    _path = str(path)

    path_elements = _path.split('/')
    path_size = len(path_elements)

    if path_size <= 1:
        return np.nan

    car_name = path_elements[path_size - 2]
    car_name_elements = car_name.split(' ')
    car_name_elements_size = len(car_name_elements)

    if car_name_elements_size <= 1:
        return np.nan

    label = car_name_elements[0]
    file_name = path_elements[path_size - 1]

    return DataEntity(label, path, file_name)
