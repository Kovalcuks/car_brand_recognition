import pandas as pd
import uuid
import os

from typing import Tuple, List
from src.domain.FileSystemPath import FileSystemPath
from src.domain.DataEntity import DataEntity
from src.utils.file_system import list_files, create_folder
from src.utils.encoders import encode_path_to_data_entity
from src.utils.image_processor import load_image, save_image, canny_filter


def process_image(path: FileSystemPath, label: str, destination: FileSystemPath):
    image = load_image(path)

    if image is None:
        return

    print('Writing image :' + label)

    destination = destination.extend(label)
    create_folder(destination)

    # You can use canny filter if its necessary otherwise comment line below
    image = canny_filter(image)

    new_file_name = str(uuid.uuid4()) + '.png'
    save_image(image, destination.extend(new_file_name))


def index_files() -> (pd.DataFrame, pd.DataFrame):
    data_entities: List[Tuple[str, str, str]] = []
    for index, file in enumerate(list_files(FileSystemPath('../dataset/car_data'))):
        data_entity: DataEntity = encode_path_to_data_entity(file)
        data_entities.append((data_entity.file_name, str(data_entity.path), str(data_entity.label)))
        print("Index: %d, Name %s, Path: %s" % (index, data_entity.file_name, data_entity.path))

    print('FileSystemPaths constructed')

    full_data = pd.DataFrame(data_entities, columns=['file_name', 'path', 'label'])

    train = full_data[full_data['path'].str.contains("train")]
    test = full_data[full_data['path'].str.contains("test")]
    print('Paths and Labels set to train/test')

    return train, test


def create_train_test_folders(train, test, train_folder, test_folder):

    if not os.path.exists(str(train_folder)):
        create_folder(train_folder)
        train[['path', 'label']] \
            .apply(lambda path_and_name:
                   process_image(FileSystemPath(path_and_name[0]), path_and_name[1], train_folder), axis=1)

    if not os.path.exists(str(test_folder)):
        create_folder(test_folder)
        test[['path', 'label']] \
            .apply(lambda path_and_name:
                   process_image(FileSystemPath(path_and_name[0]), path_and_name[1], test_folder), axis=1)
