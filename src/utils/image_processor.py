import cv2
import cv2 as cv
import  numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from src.domain.FileSystemPath import FileSystemPath


def display(path: FileSystemPath, title="Image"):
    image = mpimg.imread(str(path))
    plt.imshow(image), plt.title(title)
    plt.xticks([]), plt.yticks([])
    plt.show()


def load_image(path: FileSystemPath):
    return cv.imread(str(path))


def resize_image(image):
    if 256 is None and 256 is None:
        return image

    return cv2.resize(image, (256, 256), interpolation=cv2.INTER_AREA)


def remove_noise(image):
    return cv2.GaussianBlur(image, (9, 9), 0)


def save_image(image, path: FileSystemPath):
    cv2.imwrite(str(path), image)


def show_image(image, label):
    cv2.imshow(label, image)


def canny_filter(image):
    return cv2.Canny(image, 0, 200)


def edge_detection(image):
    vertical_filter = [[-1, -2, -1], [0, 0, 0], [1, 2, 1]]
    horizontal_filter = [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]

    n, m, d = image.shape
    edges_img = image.copy()

    for row in range(3, n - 2):
        for col in range(3, m - 2):
            local_pixels = image[row - 1:row + 2, col - 1:col + 2, 0]
            vertical_transformed_pixels = vertical_filter * local_pixels
            vertical_score = vertical_transformed_pixels.sum() / 4
            horizontal_transformed_pixels = horizontal_filter * local_pixels
            horizontal_score = horizontal_transformed_pixels.sum() / 4
            edge_score = (vertical_score ** 2 + horizontal_score ** 2) ** .5
            edges_img[row, col] = [edge_score] * 3

    return edges_img / edges_img.max()


def normalized(image):
    return cv2.normalize(image, None, 0, 255, cv2.NORM_MINMAX)


def remove_background(image):
    rectangle = (0, 56, 256, 150)

    mask = np.zeros(image.shape[:2], np.uint8)

    # Create temporary arrays used by grabCut
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)

    # Run grabCut
    cv2.grabCut(image,  # Our image
                mask,  # The Mask
                rectangle,  # Our rectangle
                bgdModel,  # Temporary array for background
                fgdModel,  # Temporary array for background
                5,  # Number of iterations
                cv2.GC_INIT_WITH_RECT)  # Initiative using our rectangle

    # Create mask where sure and likely backgrounds set to 0, otherwise 1
    mask_2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')

    # Multiply image with new mask to subtract background
    return image * mask_2[:, :, np.newaxis]

